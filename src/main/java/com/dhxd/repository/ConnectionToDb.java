package com.dhxd.repository;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionToDb {

	private static final String dbURL = "jdbc:mysql://localhost:3306/restaurant";
	private static final String userName = "root";
	private static final String password = "12345678@";

	public static Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(dbURL, userName, password);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return conn;
	}

}
