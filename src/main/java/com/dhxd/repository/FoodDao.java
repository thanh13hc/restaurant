package com.dhxd.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.dhxd.model.Food;

@Repository
public class FoodDao {
	
	public List<Food> getFoods() {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		List<Food> list = new ArrayList<Food>();
		String sql = "select * from food";

		try {
			con = (Connection) ConnectionToDb.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Food f = new Food(rs.getInt(1), rs.getString(2), rs.getInt(3));
				list.add(f);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				if (con != null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		return list;
	}
}
