package com.dhxd.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dhxd.model.Food;
import com.dhxd.repository.FoodDao;

@RestController
@RequestMapping(value = "/api")
public class HelloController {

	@Autowired
	FoodDao foodDao;

	@GetMapping(value = "/students")
	public List<Food> getList() {
		System.out.println("hello");
		List<Food> std = new ArrayList<>();
		std = foodDao.getFoods();
		return std;
	}
}
