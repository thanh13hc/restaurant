package com.dhxd.model;

import java.io.Serializable;

public class Food implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private int price;

	public Food() {
	}

	public Food(int id, String name, int price) {
		this.name = name;
		this.id = id;
		this.price = price;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
